import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient',
    getAllPatients:'/patient/allPatients',
    insertPatient: '/patient/insert',
    deletePatient: '/patient/delete/',
    updatePatient: '/patient/update/'
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.getAllPatients, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function postPatient(user, callback){
    let request = new Request(HOST.backend_api + endpoint.insertPatient , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePatient(name,callback){
    let request = new Request(HOST.backend_api + endpoint.deletePatient+name, {
        method: 'DELETE',
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


function updatePatient(name, user, callback){
    let request = new Request(HOST.backend_api + endpoint.updatePatient+name , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}



export {
    getPatients,
    postPatient,
    deletePatient,
    updatePatient,
};
