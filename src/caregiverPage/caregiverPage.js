import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'


import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import CaregiverPageContainer from './caregiverPage/caregiverPage-container'


class caregiverPage extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <Switch>

                        <Route
                            exact
                            path='/caregiverPage'
                            render={() => <CaregiverPageContainer/>}
                        />


                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default caregiverPage;